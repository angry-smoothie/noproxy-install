#!/bin/bash

if [ "$(id -u)" != "0" ]; then
        echo "Sorry, you are not root."
        exit 1
fi

add-apt-repository ppa:ubuntu-desktop/ubuntu-make
apt-get update
apt-get dist-upgrade -y
apt-get install -y ubuntu-make vim gosu
gosu $SUDO_USER umake ide atom
gosu $SUDO_USER umake nodejs
gosu $SUDO_USER umake android
gosu $SUDO_USER umake android android-ndk
gosu $SUDO_USER umake android android-sdk

