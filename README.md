# Auto install noproxy

On a new ubuntu install, run install.sh as sudoer and it will automatically install:
- docker
- docker-compose
- noproxy container
- add alias `noproxy` in `/.bashrc`

Then close your session and login.
Run `noproxy` in a shell and voila it's done!

## Precedure

1- Lancer sudo ./install.sh
2- Se reconnecter
3- Lancer noproxy
4- Lancer sudo ./install-after-relogin.sh
