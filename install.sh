#!/bin/bash

if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi

export http_proxy=http://193.56.47.8:8080
export https_proxy=${http_proxy}
curl -sSL https://get.docker.com/ | sh
curl -L "https://github.com/docker/compose/releases/download/1.9.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose

docker load -i noproxy.tar

grep ~/.bashrc noproxy &>/dev/null
if [ "$?" -ne 0 ]; then
     cat dockerfunc >> ~/.bashrc
     mkdir ~/.noproxy
     cp docker-compose.yml ~/.noproxy/
fi

source ~/.bashrc
adduser $SUDO_USER docker

